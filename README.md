libTask is a library developed to make creation of multithreaded application in C++ easy and fast. Tasks are added to a pool of tasks to be executed and threads execute them as they arrive. Tasks can have dependencies on other tasks being finished before they are executed. Tasks can also be made to fire and forget, and the library will clean up memory for you to prevent memory leaks.

Things that can be turned into tasks for improved performance:

* Blocking operations like logging or socket operations can be made into a task to be offloaded to another thread so you program doesn't have to wait for them to finish


* Algorithms can often be turned into tasks to take advantage of multithreading.

The library uses the C++11 features std::thread, std::mutex and std::chrono
The projects official page is [http://trippler.no/wpcms/?page_id=22](http://trippler.no/wpcms/?page_id=22).


Copyright (C) 2014  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.