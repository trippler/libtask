/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "task_tpm.h"
#include "../task_manager.h"


Task_tpm::Task_tpm() : Task("Tpm")
{
}

void Task_tpm::execute(Task_manager *manager)
{
	unsigned long count = manager->get_executed_task_count();
	std::this_thread::sleep_for(std::chrono::seconds(60));
	unsigned long diff = manager->get_executed_task_count() - count + 1 /*Add ourselves to calculation*/;
	printf("Tasks per minute: %lu. Total tasks executed: %lu\r\n", diff, manager->get_executed_task_count());
	manager->add_task(this);
}

void* Task_tpm::result()
{
	return 0;
}
