/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef TASK_IDLE_H
#define TASK_IDLE_H
#include <mutex>
#include <condition_variable>
#include "../task_manager.h"
#include "../task.h"
#include "task_print.h"
class Task_idle : public Task
{
public:
	Task_idle(const char* name);
	~Task_idle();
	void execute(Task_manager* manager);
	void* result();
	// Task interface
	void wake();
private:
	void set_executed(bool executed);
	std::mutex crit_section;
	std::condition_variable cond_var;
};

#endif // TASK_IDLE_H
